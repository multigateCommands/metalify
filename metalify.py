#!/usr/bin/env python3
# encoding=utf-8
from __future__ import print_function

import csv
import random
import sys
import sqlite3
import unicodedata

_unicode_db = sqlite3.connect(":memory:")
_unicode_db.execute("CREATE TABLE unicode_data (code_value TEXT, name TEXT)")

with open('UnicodeData.txt') as data:
    reader = csv.reader(data, delimiter=';', quotechar=None)
    with _unicode_db:
        for row in reader:
            _unicode_db.execute("INSERT INTO unicode_data VALUES (?, ?)", row[0:2])


if __name__ == "__main__":
    if len(sys.argv) < 2 or len(sys.argv[1]) == 0:
        print("Usage: !metalify [text]")
        sys.exit(0)

    if sys.version_info.major != 2:
        text = sys.argv[1].encode()
    else:
        text = sys.argv[1]
        chr = unichr

    try:
        text = text.decode('utf8')
    except UnicodeError:
        text = text.decode('latin1')

    out = ""
    for char in text:
        try:
            name = unicodedata.name(char)
            res = _unicode_db.execute("SELECT code_value FROM unicode_data WHERE name LIKE '%s %%'" % name)
            similar = tuple(row[0] for row in res)
            out += chr(int(random.choice(similar), 16))
        except IndexError:
            out += char

    if sys.version_info.major == 2:
        out = out.encode('utf8')
    print(out)
